const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const packageDef = protoLoader.loadSync('protos/orders.proto', {});
const grpcObject = grpc.loadPackageDefinition(packageDef);
const ordersPackage = grpcObject.ordersPackage;

const client = new ordersPackage.OrderService("localhost:40000", grpc.credentials.createInsecure());


const [_, __, userId, amount] = process.argv;


client.createOrder(
    {
        id: '',
        userId: userId,
        amount: amount,
        status: 0,
    },
    (err, response) => {
        console.log(response);
    }
);
