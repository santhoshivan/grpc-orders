const orderStatus = {
    PLACED: "PLACED",
    COMPLETED: "COMPLETED",
}
Object.freeze(orderStatus);

module.exports = orderStatus;