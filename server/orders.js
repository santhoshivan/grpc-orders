const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const ordersPackageDef = protoLoader.loadSync('protos/orders.proto', {});
const ordersGrpcObject = grpc.loadPackageDefinition(ordersPackageDef);
const ordersPackage = ordersGrpcObject.ordersPackage;
const OrderStatus = require('../constants/orderStatus');

const server = new grpc.Server();
server.bind('0.0.0.0:40000', grpc.ServerCredentials.createInsecure());


const orders = [
  {
    id: "O1",
    userId: "U1",
    amount: 199,
    status: OrderStatus.PLACED
  },
  {
    id: "O2",
    userId: "U2",
    amount: 299,
    status: OrderStatus.PLACED
  },
  {
    id: "O3",
    userId: "U3",
    amount: 399,
    status: OrderStatus.PLACED
  },
];


const createOrder = (call, callback) => {
  const order = {
    ...call.request,
    id : 'O' + (orders.length+1).toString(),
    status : OrderStatus.PLACED,
  };
  orders.push(order);
  callback(null, order);
};


const getOrders = (call, callback) => {
  const ordersResponse = {
    orders: orders,
  };
  callback(null, ordersResponse);
};

const getOrdersByUserId = (call, callback) => {
  const { userId } = call.request;
  const userOrders = orders.filter((order) => order.userId === userId);
  const ordersResponse = {
    orders: userOrders,
  }
  callback(null, ordersResponse);
}


const updateOrder = (call, callback) => {
  const { id, status } = call.request;
  const orderIndex = orders.findIndex((order) => order.id === id);
  orders[orderIndex] = {
    ...orders[orderIndex],
    status: status,
  };
  callback(null, orders[orderIndex]);
};

const deleteOrder = (call, callback) => {
  const { id } = call.request;
  const orderIndex = orders.findIndex((order) => order.id == id);
  const order = orderIndex < 0 ? null : orders[orderIndex]
  const message = orderIndex < 0 ? 'Order Not Found' : `Deleted Order ${order.id}`;
  const response = {
    order: order,
    message: message,
  };
  orders.splice(orderIndex, 1);
  callback(null, response);
}

server.addService(ordersPackage.OrderService.service, {
    'createOrder': createOrder,
    'getOrders': getOrders,
    'updateOrder': updateOrder,
    'deleteOrder': deleteOrder,
    'getOrdersByUserId': getOrdersByUserId,
});

server.start();