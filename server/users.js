const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const usersPackageDef = protoLoader.loadSync('protos/users.proto', {});
const usersGrpcObject = grpc.loadPackageDefinition(usersPackageDef);
const usersPackage = usersGrpcObject.usersPackage;

const packageDef = protoLoader.loadSync('protos/orders.proto', {});
const grpcObject = grpc.loadPackageDefinition(packageDef);
const ordersPackage = grpcObject.ordersPackage;
const client = new ordersPackage.OrderService("localhost:40000", grpc.credentials.createInsecure());

const server = new grpc.Server();
server.bind('0.0.0.0:20000', grpc.ServerCredentials.createInsecure());

const users = [
  {
    userId: "U1",
    name: "Santhoshivan"
  },
  {
    userId: "U2",
    name: "Akshay"
  },
];


const getOrdersByName = (call, callback) => {
    console.time('getOrdersByName');
    const userId   = users.find((user) => user.name === call.request.name).userId;
    const orderRequest = {
        userId: userId,
    };
    client.getOrdersByUserId(orderRequest, (err, response) => {
      if(err) {

      }
      callback(null, response);
    })
    console.timeEnd('getOrdersByName');
};

server.addService(usersPackage.UserService.service, {
    'getOrdersByName': getOrdersByName,
});

server.start();